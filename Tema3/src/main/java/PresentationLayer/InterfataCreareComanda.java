package PresentationLayer;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import BusinessLogic.LogicComanda;
import DataAccess.Conexiune;

public class InterfataCreareComanda {
	private JFrame frame = new JFrame("Plasare comanda");
	private JLabel info= new JLabel("Completati urmatoarele informatii:");
	private JComboBox marime1= new JComboBox(new String[]{"","S","M", "L"});
	private JLabel marime= new JLabel("Marime: ");
	private JLabel voucher= new JLabel("Voucher: ");
	private JLabel numeProdus= new JLabel("Numele produsului: ");
	private JLabel idClient= new JLabel("Id-ul clientului: ");
	private JButton enter= new JButton("Enter");
	private JLabel cantitate= new JLabel("Numar produse: ");
	
	private JComboBox<String> tNumeProdus=new JComboBox(new String[]{""});
	private JTextField tIdClient= new JTextField("");
	private JTextField reducere= new JTextField("procent reducere");
	private JTextField tVoucher= new JTextField("cod");
	private JTextField tCantitate= new JTextField("");
	private LogicComanda l=new LogicComanda();
	InterfataCreareComanda() throws ClassNotFoundException, SQLException{
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(550, 450);
		setCombo();
		
		enter.setFont(new Font("Arial",Font.PLAIN,25));
		info.setFont(new Font("Arial",Font.PLAIN,25));
		marime.setFont(new Font("Arial",Font.PLAIN,25));
		marime1.setFont(new Font("Arial",Font.PLAIN,25));
		numeProdus.setFont(new Font("Arial",Font.PLAIN,25));
		idClient.setFont(new Font("Arial",Font.PLAIN,25));
		cantitate.setFont(new Font("Arial",Font.PLAIN,25));
		reducere.setFont(new Font("Arial",Font.PLAIN,25));
		voucher.setFont(new Font("Arial",Font.PLAIN,25));
		
		tNumeProdus.setFont(new Font("Arial",Font.PLAIN,25));
		tIdClient.setFont(new Font("Arial",Font.PLAIN,25));
		tVoucher.setFont(new Font("Arial",Font.PLAIN,25));
		tCantitate.setFont(new Font("Arial",Font.PLAIN,25));
		
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	    
		
		JPanel pol= new JPanel();
	    pol.setLayout(new BoxLayout(pol, BoxLayout.X_AXIS));
	    pol.add(info);
	    
	    JPanel pol1 = new JPanel();
	    pol1.setLayout(new BoxLayout(pol1, BoxLayout.X_AXIS));
	    pol1.add( Box.createRigidArea(new Dimension(50,0)) );
	    pol1.add(idClient);
	    pol1.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol1.add( tIdClient);
	    pol1.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    
	    JPanel pol2 = new JPanel();
	    pol2.setLayout(new BoxLayout(pol2, BoxLayout.X_AXIS));
	    pol2.add( Box.createRigidArea(new Dimension(50,0)) );
	    pol2.add(numeProdus);
	    pol2.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol2.add(tNumeProdus);
	    pol2.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    JPanel pol3 = new JPanel();
	    pol3.setLayout(new BoxLayout(pol3, BoxLayout.X_AXIS));
	    pol3.add( Box.createRigidArea(new Dimension(50,0)) );
	    pol3.add(marime);
	    pol3.add( Box.createRigidArea(new Dimension(20,0)) );
	    pol3.add(marime1);
	    pol3.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    JPanel pol4= new JPanel();
	    pol4.setLayout(new BoxLayout(pol4, BoxLayout.X_AXIS));
	    pol4.add( Box.createRigidArea(new Dimension(50,0)) );
	    pol4.add(cantitate);
	    pol4.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol4.add(tCantitate);
	    pol4.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    JPanel pol5= new JPanel();
	    pol5.setLayout(new BoxLayout(pol5, BoxLayout.X_AXIS));
	    pol5.add( Box.createRigidArea(new Dimension(50,0)) );
	    pol5.add(voucher);
	    pol5.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol5.add(reducere);
	    pol5.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol5.add(tVoucher);
	    pol5.add( Box.createRigidArea(new Dimension(100,0)) );
	    
		
		JPanel pol12= new JPanel();
		pol12.setLayout(new BoxLayout(pol12, BoxLayout.X_AXIS));
		pol12.add(enter);
		
		panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol1);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol2);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol3);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol4);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol5);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol12);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    
	    frame.add(panel);
	    frame.setVisible(true);
		enter.addActionListener(new creareComanda());
	}
	private void setCombo() throws ClassNotFoundException, SQLException {
		
		
		String query= String.format("select * from Produs; ");
		Connection con= Conexiune.getConnection();
		Statement stm=null;
		ResultSet rs=null;
		stm = con.createStatement();
		rs=stm.executeQuery(query);
		while(rs.next()==true) {
			tNumeProdus.addItem(rs.getString("nume"));
		}
		stm.close();
		con.close();
		rs.close();
	}
	
	private class creareComanda implements ActionListener {
		public void actionPerformed(ActionEvent e){
			try {
				String s= (String) marime1.getSelectedItem();
				String np= (String) tNumeProdus.getSelectedItem();
				
			l.adaugareComanda(tIdClient.getText(), np, s, tCantitate.getText(), tVoucher.getText(), reducere.getText());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
	
}
