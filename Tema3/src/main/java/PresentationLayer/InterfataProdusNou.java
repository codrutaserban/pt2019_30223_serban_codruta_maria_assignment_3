package PresentationLayer;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import BusinessLogic.Logic;

public class InterfataProdusNou {


		private JFrame frame = new JFrame("Adaugare produs");
		private JLabel info= new JLabel("Completati urmatoarele campuri:");
		private JLabel nume= new JLabel("Nume: ");
		private JLabel cantitate= new JLabel("Cantitate: ");
		private JLabel pret= new JLabel("Pret: ");

		
		private JTextField tNume= new JTextField("");
		private JTextField tCantitate= new JTextField("");
		private JTextField tPret= new JTextField("");
		
		private JButton enter= new JButton("Enter");
		
		Logic l= new Logic();
		

		InterfataProdusNou(){
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.setSize(550, 350);
			
			enter.setFont(new Font("Arial",Font.PLAIN,25));
			info.setFont(new Font("Arial",Font.PLAIN,25));
			nume.setFont(new Font("Arial",Font.PLAIN,25));
			cantitate.setFont(new Font("Arial",Font.PLAIN,25));
			pret.setFont(new Font("Arial",Font.PLAIN,25));
			
			
			tNume.setFont(new Font("Arial",Font.PLAIN,25));
			tCantitate.setFont(new Font("Arial",Font.PLAIN,25));
			tPret.setFont(new Font("Arial",Font.PLAIN,25));
			
			JPanel panel= new JPanel();
		    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		    
		    JPanel pol1 = new JPanel();
		    pol1.setLayout(new BoxLayout(pol1, BoxLayout.X_AXIS));
		    pol1.add( Box.createRigidArea(new Dimension(30,0)) );
		    pol1.add(nume);
		    pol1.add( Box.createRigidArea(new Dimension(15,0)) );
		    pol1.add( tNume);
		    pol1.add( Box.createRigidArea(new Dimension(100,0)) );
		    
		    
		    JPanel pol2 = new JPanel();
		    pol2.setLayout(new BoxLayout(pol2, BoxLayout.X_AXIS));
		    pol2.add( Box.createRigidArea(new Dimension(30,0)) );
		    pol2.add(cantitate);
		    pol2.add( Box.createRigidArea(new Dimension(15,0)) );
		    pol2.add(tCantitate);
		    pol2.add( Box.createRigidArea(new Dimension(100,0)) );
		    
		    JPanel pol3 = new JPanel();
		    pol3.setLayout(new BoxLayout(pol3, BoxLayout.X_AXIS));
		    pol3.add( Box.createRigidArea(new Dimension(30,0)) );
		    pol3.add(pret);
		    pol3.add( Box.createRigidArea(new Dimension(15,0)) );
		    pol3.add(tPret);
		    pol3.add( Box.createRigidArea(new Dimension(100,0)) );
		    
		  
		    
		    JPanel pol= new JPanel();
		    pol.setLayout(new BoxLayout(pol, BoxLayout.X_AXIS));
		    pol.add(info);
		    
		    JPanel pol12= new JPanel();
		    pol12.setLayout(new BoxLayout(pol12, BoxLayout.X_AXIS));
		    pol12.add(enter);
		    
		    panel.add( Box.createRigidArea(new Dimension(0,20)) );
		    panel.add(pol);
		    panel.add( Box.createRigidArea(new Dimension(0,20)) );
		    panel.add(pol1);
		    panel.add( Box.createRigidArea(new Dimension(0,20)) );
		    panel.add(pol2);
		    panel.add( Box.createRigidArea(new Dimension(0,20)) );
		    panel.add(pol3);
		    panel.add( Box.createRigidArea(new Dimension(0,20)) );
		    panel.add( Box.createRigidArea(new Dimension(0,20)) );
		    panel.add(pol12);
		    panel.add( Box.createRigidArea(new Dimension(0,20)) );
		    frame.add(panel);
		    frame.setVisible(true);
		    
		   enter.addActionListener(new pAdaugare());
		}
		private class pAdaugare implements ActionListener {
			public void actionPerformed(ActionEvent e){
				try {
				l.addProdus(tNume.getText(), tCantitate.getText(), tPret.getText());	
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}

}
