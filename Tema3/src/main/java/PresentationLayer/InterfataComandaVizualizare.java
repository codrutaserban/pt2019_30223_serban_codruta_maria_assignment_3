package PresentationLayer;

import java.awt.FlowLayout;
import java.awt.Font;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Model.Comanda;

public class InterfataComandaVizualizare {
	private JTable tabel=new JTable();
	private DefaultTableModel model = new DefaultTableModel();
	private Object[] capDeTabel = new Object[4];
	private Object[] date = new Object[4];
	
	private JFrame frame = new JFrame("Vizualizare comenzi"); 
	public InterfataComandaVizualizare () throws ClassNotFoundException, SQLException{
		Comanda comanda=new Comanda();
		JScrollPane scroll;
		JPanel panel=new JPanel();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(500, 480);
		capDeTabel[0]="id";
		capDeTabel[1]="Data comanda";
		capDeTabel[2]="Data livrare";
		capDeTabel[3]="Total";
		model.setColumnIdentifiers(capDeTabel);
		int n= comanda.getComenzi().size();
		try {
			for(int i=0; i<n;i++)
			{
				date[0]=comanda.getComanda(i).getId();
				date[1]=comanda.getComanda(i).getDataEfectuare();
				date[2]=comanda.getComanda(i).getDataLivrare();
				date[3]=comanda.getComanda(i).getTotal();
				model.addRow(date);
			}
		} catch (Exception e) {}
		
		
		tabel.setModel(model);
		tabel.setFont(new Font("Arial",Font.PLAIN,12));
		tabel.setEnabled(false);
		scroll= new JScrollPane(tabel);
		panel.add(scroll,new FlowLayout());
		frame.add(panel);
		frame.setVisible(true);
		
	}
}
