package PresentationLayer;

import java.awt.FlowLayout;
import java.awt.Font;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Model.Produs;

public class InterfataProdusVizualizare {

	
	
	private JTable tabel=new JTable();
	private DefaultTableModel model = new DefaultTableModel();
	private Object[] capDeTabel = new Object[4];
	private Object[] date = new Object[4];
	
	private JFrame frame = new JFrame("Vizualizare produse"); 
	
	public InterfataProdusVizualizare () throws ClassNotFoundException, SQLException{
	Produs produs=new Produs();
	JScrollPane scroll;
	JPanel panel=new JPanel();
	frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	frame.setSize(500, 480);
	capDeTabel[0]="id";
	capDeTabel[1]="Nume";
	capDeTabel[2]="Cantitate";
	capDeTabel[3]="Pret";
	model.setColumnIdentifiers(capDeTabel);
	int n= produs.getProduse().size();
	try {
		for(int i=0; i<n;i++)
		{
			date[0]=produs.getProdus(i).getId();
			date[1]=produs.getProdus(i).getNume();
			date[2]=produs.getProdus(i).getCantitate();
			date[3]=produs.getProdus(i).getPret();
			model.addRow(date);
		}
	} catch (Exception e) {}
	
	
	tabel.setModel(model);
	tabel.setFont(new Font("Arial",Font.PLAIN,12));
	tabel.setEnabled(false);
	scroll= new JScrollPane(tabel);
	panel.add(scroll,new FlowLayout());
	frame.add(panel);
	frame.setVisible(true);
	
	}
}
