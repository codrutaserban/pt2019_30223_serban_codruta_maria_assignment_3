package PresentationLayer;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Model.Client;

public class InterfataVizualizareClient {

	
	
	private JTable tabel=new JTable();
	private DefaultTableModel model = new DefaultTableModel();
	private Object[] capDeTabel = new Object[6];
	private Object[] date = new Object[6];
	
	private JFrame frame = new JFrame("Vizualizare clienti"); 
	
	public InterfataVizualizareClient() throws ClassNotFoundException, SQLException{
	Client client=new Client();	
	JScrollPane scroll;
	JPanel panel=new JPanel();
	frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	frame.setSize(500, 480);
	capDeTabel[0]="id";
	capDeTabel[1]="Nume";
	capDeTabel[2]="Prenume";
	capDeTabel[3]="Email";
	capDeTabel[4]="Telefon";
	capDeTabel[5]="Sex";
	model.setColumnIdentifiers(capDeTabel);
	int n= client.getClienti().size();
	try {
		for(int i=0; i<n;i++)
		{
			date[0]=client.getCl(i).getId();
			date[1]=client.getCl(i).getNume();
			date[2]=client.getCl(i).getPrenume();
			date[3]=client.getCl(i).getEmail();
			date[4]=client.getCl(i).getTelefon();
			date[5]=client.getCl(i).getSex();
			model.addRow(date);
		}
	} catch (Exception e) {}
	
	
	tabel.setModel(model);
	tabel.setFont(new Font("Arial",Font.PLAIN,12));
	tabel.setEnabled(false);
	scroll= new JScrollPane(tabel);
	panel.add(scroll,new FlowLayout());
	frame.add(panel);
	frame.setVisible(true);
	
	}
}


	

	
	




