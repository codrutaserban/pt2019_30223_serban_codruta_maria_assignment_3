package PresentationLayer;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;


public class InterfataComanda {
	private JFrame frame=new JFrame("Comanda");
	private JButton viz= new JButton("Afisare comenzi");
	private JButton comanda= new JButton("Creare comanda");
	
	InterfataComanda(){
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(450, 200);
		viz.setFont(new Font("Arial",Font.PLAIN,25));
		comanda.setFont(new Font("Arial",Font.PLAIN,25));
		
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	    panel.add( Box.createRigidArea(new Dimension(150,25)) );
	    panel.add(comanda);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(viz);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    
	    frame.add(panel);
		frame.setVisible(true);
		comanda.addActionListener(new opCreare());
	 	viz.addActionListener(new opViz());
	}
	private class opCreare implements ActionListener {
 		public void actionPerformed(ActionEvent e){
 				try {
					InterfataCreareComanda produsAd=new InterfataCreareComanda();
				} catch (Exception e1) {
					e1.printStackTrace();
				} 
 		}
 	}
	
	
	private class opViz implements ActionListener {
 		public void actionPerformed(ActionEvent e){
 			try {
				InterfataComandaVizualizare produsViz=new InterfataComandaVizualizare();
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
 		}
 	}
}
