package PresentationLayer;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import BusinessLogic.Logic;



public class InterfataClientNou {
	private JFrame frame = new JFrame("Adaugare client");
	private JLabel info= new JLabel("Completati urmatoarele campuri:");
	private JLabel nume= new JLabel("Nume: ");
	private JLabel prenume= new JLabel("Prenume: ");
	private JLabel telefon= new JLabel("Telefon: ");
	private JLabel email= new JLabel("Email: ");
	private JLabel sex= new JLabel("Sex: ");
	
	private JTextField tNume= new JTextField("");
	private JTextField tPrenume= new JTextField("");
	private JTextField tTelefon= new JTextField("");
	private JTextField tEmail= new JTextField("");
	private JTextField tSex= new JTextField("F/M");
	
	private JLabel codPostal= new JLabel("Cod postal: ");
	private JLabel judet= new JLabel("Judet: ");
	private JLabel oras= new JLabel("Oras: ");
	private JLabel strada= new JLabel("Strada: ");
	private JLabel numar= new JLabel("Numar: ");
	
	private JTextField tCodPostal= new JTextField("");
	private JTextField tJudet= new JTextField("");
	private JTextField tOras= new JTextField("");
	private JTextField tStrada= new JTextField("");
	private JTextField tNumar= new JTextField("");
	
	private JButton enter= new JButton("Enter");
	
	private Logic l= new Logic();

	InterfataClientNou(){
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(550, 750);
		
		enter.setFont(new Font("Arial",Font.PLAIN,25));
		info.setFont(new Font("Arial",Font.PLAIN,25));
		nume.setFont(new Font("Arial",Font.PLAIN,25));
		prenume.setFont(new Font("Arial",Font.PLAIN,25));
		telefon.setFont(new Font("Arial",Font.PLAIN,25));
		email.setFont(new Font("Arial",Font.PLAIN,25));
		sex.setFont(new Font("Arial",Font.PLAIN,25));
		
		tNume.setFont(new Font("Arial",Font.PLAIN,25));
		tPrenume.setFont(new Font("Arial",Font.PLAIN,25));
		tTelefon.setFont(new Font("Arial",Font.PLAIN,25));
		tEmail.setFont(new Font("Arial",Font.PLAIN,25));
		tSex.setFont(new Font("Arial",Font.PLAIN,25));
		
		codPostal.setFont(new Font("Arial",Font.PLAIN,25));
		judet.setFont(new Font("Arial",Font.PLAIN,25));
		oras.setFont(new Font("Arial",Font.PLAIN,25));
		strada.setFont(new Font("Arial",Font.PLAIN,25));
		numar.setFont(new Font("Arial",Font.PLAIN,25));
		
		tCodPostal.setFont(new Font("Arial",Font.PLAIN,25));
		tJudet.setFont(new Font("Arial",Font.PLAIN,25));
		tOras.setFont(new Font("Arial",Font.PLAIN,25));
		tStrada.setFont(new Font("Arial",Font.PLAIN,25));
		tNumar.setFont(new Font("Arial",Font.PLAIN,25));
		
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	    
	    JPanel pol1 = new JPanel();
	    pol1.setLayout(new BoxLayout(pol1, BoxLayout.X_AXIS));
	    pol1.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol1.add(nume);
	    pol1.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol1.add( tNume);
	    pol1.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    
	    JPanel pol2 = new JPanel();
	    pol2.setLayout(new BoxLayout(pol2, BoxLayout.X_AXIS));
	    pol2.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol2.add(prenume);
	    pol2.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol2.add(tPrenume);
	    pol2.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    JPanel pol3 = new JPanel();
	    pol3.setLayout(new BoxLayout(pol3, BoxLayout.X_AXIS));
	    pol3.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol3.add(telefon);
	    pol3.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol3.add(tTelefon);
	    pol3.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    
	    JPanel pol4= new JPanel();
	    pol4.setLayout(new BoxLayout(pol4, BoxLayout.X_AXIS));
	    pol4.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol4.add(email);
	    pol4.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol4.add(tEmail);
	    pol4.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    JPanel pol5= new JPanel();
	    pol5.setLayout(new BoxLayout(pol5, BoxLayout.X_AXIS));
	    pol5.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol5.add(sex);
	    pol5.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol5.add(tSex);
	    pol5.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    JPanel pol6= new JPanel();
	    pol6.setLayout(new BoxLayout(pol6, BoxLayout.X_AXIS));
	    pol6.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol6.add(codPostal);
	    pol6.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol6.add(tCodPostal);
	    pol6.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    JPanel pol7= new JPanel();
	    pol7.setLayout(new BoxLayout(pol7, BoxLayout.X_AXIS));
	    pol7.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol7.add(judet);
	    pol7.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol7.add(tJudet);
	    pol7.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    JPanel pol8= new JPanel();
	    pol8.setLayout(new BoxLayout(pol8, BoxLayout.X_AXIS));
	    pol8.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol8.add(oras);
	    pol8.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol8.add(tOras);
	    pol8.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    JPanel pol9= new JPanel();
	    pol9.setLayout(new BoxLayout(pol9, BoxLayout.X_AXIS));
	    pol9.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol9.add(strada);
	    pol9.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol9.add(tStrada);
	    pol9.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    JPanel pol10= new JPanel();
	    pol10.setLayout(new BoxLayout(pol10, BoxLayout.X_AXIS));
	    pol10.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol10.add(numar);
	    pol10.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol10.add(tNumar);
	    pol10.add( Box.createRigidArea(new Dimension(100,0)) );
	    
	    JPanel pol= new JPanel();
	    pol.setLayout(new BoxLayout(pol, BoxLayout.X_AXIS));
	    pol.add(info);
	    
	    JPanel pol12= new JPanel();
	    pol12.setLayout(new BoxLayout(pol12, BoxLayout.X_AXIS));
	    pol12.add(enter);
	    
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol1);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol2);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol3);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol4);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol5);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol6);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol7);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol8);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol9);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol10);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(pol12);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    frame.add(panel);
	    frame.setVisible(true);
	    enter.addActionListener(new cAdaugare());
	}
	private class cAdaugare implements ActionListener {
		public void actionPerformed(ActionEvent e){
			try {
				l.addClient(tNume.getText(), tPrenume.getText(),tTelefon.getText(), tEmail.getText(), tSex.getText(), tCodPostal.getText(), tJudet.getText(), tOras.getText(), tStrada.getText(), tNumar.getText());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
}
