package PresentationLayer;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class InterfataClient {
	private JButton adaugare= new JButton("Adaugare client"); 
	private JButton editare=new JButton("Editare client");
	private JButton stergere= new JButton("Stergere client");
	private JButton vizualizare=new JButton("Vizualizare clienti");
	private JFrame frame = new JFrame("Client");
	
	
	
	public InterfataClient (){
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(450, 325);
		
		
		adaugare.setFont(new Font("Arial",Font.PLAIN,25));
		editare.setFont(new Font("Arial",Font.PLAIN,25));
		stergere.setFont(new Font("Arial",Font.PLAIN,25));
		vizualizare.setFont(new Font("Arial",Font.PLAIN,25));
		
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	    panel.add( Box.createRigidArea(new Dimension(150,25)) );
	    panel.add(adaugare);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(editare);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(stergere);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
		panel.add(vizualizare);
		panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    frame.add(panel);
	    frame.setVisible(true);
	    
	    adaugare.addActionListener(new opAdaugare());
		editare.addActionListener(new opEditare());
		stergere.addActionListener(new opStergere());
		vizualizare.addActionListener(new opViz());
	}
	
	private class opAdaugare implements ActionListener {
		public void actionPerformed(ActionEvent e){
				InterfataClientNou clientAd=new InterfataClientNou();
		}
	}
	
	private class opEditare implements ActionListener {
		public void actionPerformed(ActionEvent e){
				InterfataClientEditare clientEd=new InterfataClientEditare();
		}
	}
	
	private class opStergere implements ActionListener {
		public void actionPerformed(ActionEvent e){
			InterfataClientStergere clientSt=new InterfataClientStergere();
		}
	}
	private class opViz implements ActionListener {
		public void actionPerformed(ActionEvent e){
			try {
				InterfataVizualizareClient cclientViz=new InterfataVizualizareClient();
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

}
