package PresentationLayer;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class InterfataPornire {
	
	private JFrame frame;
	private JTextArea mesaj= new JTextArea("     Alegeti o optiune:");
	private JButton produs= new JButton("Produs");
	private JButton client= new JButton("Client");
	private JButton comanda= new JButton("Comanda");
	
	
	
	public InterfataPornire(){
		mesaj.setEditable(false);
		frame = new JFrame("Prima pagina");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(350, 325);
		mesaj.setFont(new Font("Arial",Font.PLAIN,25));
		produs.setFont(new Font("Arial",Font.PLAIN,25));
		client.setFont(new Font("Arial",Font.PLAIN,25));
		comanda.setFont(new Font("Arial",Font.PLAIN,25));
		
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(mesaj);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(client);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(produs);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
		panel.add(comanda);
		panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    frame.add(panel);
		frame.setVisible(true);
		produs.addActionListener(new opProdus());
		client.addActionListener(new opClient());
		comanda.addActionListener(new opComanda());
	}
	private class opProdus implements ActionListener {
		public void actionPerformed(ActionEvent e){
				InterfataProdus produs=new InterfataProdus();
		}
	}
	
	private class opClient implements ActionListener {
		public void actionPerformed(ActionEvent e){
				InterfataClient client=new InterfataClient();
		}
	}
	
	private class opComanda implements ActionListener {
		public void actionPerformed(ActionEvent e){
				InterfataComanda comanda=new InterfataComanda();
		}
	}
	
}
