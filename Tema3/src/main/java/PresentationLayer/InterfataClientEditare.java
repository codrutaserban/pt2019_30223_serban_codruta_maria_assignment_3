package PresentationLayer;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import BusinessLogic.Logic;

public class InterfataClientEditare {

	private JFrame frame = new JFrame("Editare client");
	private JLabel info= new JLabel("Completati urmatoarele informatii:");
	private JComboBox cb= new JComboBox(new String[]{"","Nume","Prenume", "Telefon", "Email", "Sex","Cod postal","Judet","Oras","Strada","Numar"});
	private JLabel camp= new JLabel("Camp: ");
	private JLabel idClient= new JLabel("Id-ul clientului: ");
	private JLabel val= new JLabel("Noua valoare: ");
	private JButton enter= new JButton("Enter");
	
	private JTextField tIdClient= new JTextField("");
	private JTextField tVal= new JTextField("");
	Logic l=new Logic();
	
	InterfataClientEditare(){
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(550, 480);
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
	    info.setFont(new Font("Arial",Font.PLAIN,25));
	    camp.setFont(new Font("Arial",Font.PLAIN,25));
	    cb.setFont(new Font("Arial",Font.PLAIN,25));
	    idClient.setFont(new Font("Arial",Font.PLAIN,25));
	    val.setFont(new Font("Arial",Font.PLAIN,25));
	    tIdClient.setFont(new Font("Arial",Font.PLAIN,25));
	    tVal.setFont(new Font("Arial",Font.PLAIN,25));
	    enter.setFont(new Font("Arial",Font.PLAIN,25));
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(info);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(idClient);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	   panel.add(tIdClient);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(camp);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(cb);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(val);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(tVal);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
	    panel.add(enter);
	    panel.add( Box.createRigidArea(new Dimension(0,20)) );
		frame.add(panel);
		frame.setVisible(true);
		enter.addActionListener(new cEditare());
	}
	private class cEditare implements ActionListener {
		public void actionPerformed(ActionEvent e){
			try {
				String s= (String) cb.getSelectedItem();
				
			l.editClient(tIdClient.getText(),s, tVal.getText());	
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
}
