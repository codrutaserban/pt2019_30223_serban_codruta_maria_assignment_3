package BusinessLogic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JOptionPane;

import DataAccess.Conexiune;

public class LogicComanda {
	/**
	 * Metoda returneaza valoarea totala a comenzii calculata in functie de numarul de produse dar si de valoarea unui vaoucher optional
	 * @param numeProdus
	 * @param cantitate
	 * @param reducere
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public String calcTotal(String numeProdus, int cantitate,int reducere) throws SQLException, ClassNotFoundException {	
		Double total=0.0;
		String sql= String.format("select pret from Produs where nume='%s';", numeProdus);
		Connection con= Conexiune.getConnection();
		Statement stm=con.createStatement();
		ResultSet rs=stm.executeQuery(sql);
		rs.next();
		total= (double) (cantitate*rs.getInt(1));
		stm.close();
		con.close();
		rs.close();
		if(reducere>0) {
			total=total-total*reducere/100.00;
		}
		return String.format("%.2f",total);
		}
	
	/**
	 * 	
	 * @param id
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * Verifica daca un client cu un anumit id se afla in baza de date
	 */
		public int validareClient(String id) throws SQLException, ClassNotFoundException{
			Connection con= Conexiune.getConnection();
			Statement stm=null;
			ResultSet rs=null;
			String query= String.format("select id from Client where id='%s';",id);
			stm=con.createStatement();
			rs=stm.executeQuery(query);
			if(rs.next()==false)
					{
					JOptionPane.showMessageDialog(null, "Clientul nu a fost gasit!");
					return-1;
					}
			stm.close();
			rs.close();
			con.close();
			return 0;
		}
	
/**
 * Metoda se foloseste la crearea unei comenzi pentru a se decrementa stocul din produsul comandat	
 * @param numeProdus
 * @param cantitate
 * @return
 * @throws SQLException
 * @throws ClassNotFoundException
 */
	public int updateCantitate(String numeProdus, int cantitate) throws SQLException, ClassNotFoundException{
		String query=String.format("select cantitate from Produs where nume='%s' ; ",numeProdus);
		Connection con= Conexiune.getConnection();
		Statement stm=con.createStatement();
		ResultSet rs=stm.executeQuery(query);
		if(rs.next()==false ){
			JOptionPane.showMessageDialog(null, "Produsul nu a fost gasit!");
			stm.close();
			con.close();
			rs.close();
			return -1;
		}else {
			if (cantitate>rs.getInt(1)) {
				JOptionPane.showMessageDialog(null, "Stocul produsului este insuficient pentru comanda dorita!");
				stm.close();
				con.close();
				rs.close();
				return -1 ;
			}
			else {query=String.format("update Produs set Produs.cantitate='%d' where Produs.nume='%s' ;",rs.getInt(1)-cantitate,numeProdus);
			stm=con.createStatement();
			stm.executeUpdate(query);	
			}
		}
		stm.close();
		con.close();
		rs.close();
		return 0;
	}
	
	/**
	 * Se genereaza factura comenzii
	 * @param idClient
	 * @param idProdus
	 * @param idComanda
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void factura(int idClient, int idProdus, int idComanda) throws SQLException, ClassNotFoundException, IOException{
		 File f= new File(String.format("Factura%d.txt",idComanda));
		 BufferedWriter writer = null;
		 try {writer = new BufferedWriter(new FileWriter(f));}
		catch(Exception e){JOptionPane.showMessageDialog(null, "Eroare la crearea fisierului!");return;}
		Connection con= Conexiune.getConnection();
		String query,sql1,sql=String.format("select * from Client where id='%d';",idClient);
		Statement stm=con.createStatement();
		Statement stm1=con.createStatement();
		ResultSet rs1,rs=stm.executeQuery(sql);
		rs.next();
		String titlu= String.format("               FACTURA NR. %d\n", idComanda);
		String client=String.format("Destinatar: %s %s\nTelefon: %s\nEmail: %s\n", rs.getString("nume"), rs.getString("prenume"), rs.getString("telefon"), rs.getString("email"));
		sql=String.format("select * from Detalii where idComanda='%d';",idComanda);
		query=String.format("select * from Produs where id='%d';",idProdus);
		sql1=String.format("select * from Comanda where id='%d';",idComanda);
		rs1=stm1.executeQuery(query);
		rs1.next();
		stm=con.createStatement();
		rs=stm.executeQuery(sql);
		rs.next();
		String produs=String.format("Nume produs: %s\nNumar bucati: %d\nMarime: %s\nVoucher: %d, cod %s \n", rs1.getString("nume"),rs.getInt("cantitate"),rs.getString("marime"),rs.getInt("voucher"),rs.getString("cod"));
		stm1.close();
		stm1=con.createStatement();
		rs1=stm1.executeQuery(sql1);
		rs1.next();
		String total= String.format("TOTAL:   lei",rs1.getString("total"));
		sql=String.format("select * from Adresa where idClient='%d';",idClient);
		stm.close();
		stm=con.createStatement();
		rs=stm.executeQuery(sql);
		rs.next();
		String text=String.format("    Comanda efectuata la data %s va fi livrata in data de"+"\n"+"%s, la adresa %s %s %s %s %s.\n Va multumim pentru alegerea facuta!",rs1.getString("dataEfectuare"),rs1.getString("dataLivrare"),rs.getString("judet"),rs.getString("oras"),rs.getString("strada"),rs.getString("numar"),rs.getString("codPostal"));
		writer.write(titlu+client+produs+total+text);
		try {writer.close();} 
		catch (Exception e) {}}
	
	/**
	 * Se efectueaza o comanda, metoda apeleaza alte metode:validare client, updateCantitate si factura
	 * @param idClient
	 * @param numeProdus
	 * @param marime
	 * @param cantitate
	 * @param voucher
	 * @param reducere
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public  void adaugareComanda(String idClient, String numeProdus,String marime, String cantitate, String voucher, String reducere) throws SQLException, ClassNotFoundException, IOException
	{
		Connection con= Conexiune.getConnection();
		int idProdus,idComanda=0;
		String dataCurenta,dataLivrare;
		if(validareClient(idClient)==-1) return;
		if(updateCantitate(numeProdus,Integer.parseInt(cantitate))==-1) return;
		String query=String.format("select id from Produs where nume='%s';",numeProdus);
		Statement stm=con.createStatement();
		ResultSet rs=stm.executeQuery(query);
		rs.next();
		idProdus=rs.getInt(1);
		GregorianCalendar gcalendar = new GregorianCalendar();
		gcalendar.set(gcalendar.get(Calendar.YEAR), gcalendar.get(Calendar.MONTH), gcalendar.get(Calendar.DATE) );
		dataCurenta= String.format("%d.%d.%d",gcalendar.get(Calendar.DATE),gcalendar.get(Calendar.MONTH)+1,gcalendar.get(Calendar.YEAR));
		gcalendar.set(gcalendar.get(Calendar.YEAR), gcalendar.get(Calendar.MONTH), gcalendar.get(Calendar.DATE) +3);
		dataLivrare= String.format("%d.%d.%d",gcalendar.get(Calendar.DATE),gcalendar.get(Calendar.MONTH)+1,gcalendar.get(Calendar.YEAR));
		query=String.format("insert into Comanda (idClient,total,dataLivrare,dataEfectuare) values ('%s','%s','%s','%s');",idClient,calcTotal(numeProdus,Integer.parseInt(cantitate),Integer.parseInt(reducere)),dataLivrare,dataCurenta);
		stm.executeUpdate(query);
		stm=con.createStatement();
		query=String.format("select id from Comanda where idClient='%s;'", idClient);
		rs=stm.executeQuery(query);
		rs.next();
		idComanda=rs.getInt("id");
		query=String.format("insert into Detalii (idComanda,idProdus,marime,cantitate,cod,voucher) values ('%d','%d','%s','%s','%s','%s');",idComanda,idProdus,marime,cantitate,voucher,reducere);
		stm.executeUpdate(query);
		factura(Integer.parseInt(idClient),idProdus,idComanda);
		stm.close();
		con.close();
		rs.close();
	}
	
}
