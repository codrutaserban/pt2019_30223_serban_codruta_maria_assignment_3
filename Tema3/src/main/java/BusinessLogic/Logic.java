package BusinessLogic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JOptionPane;

import DataAccess.Conexiune;

public class Logic {

	
/**
 * 	
 * @param nume
 * @param prenume
 * @param telefon
 * @param email
 * @param sex
 * @param codPostal
 * @param judet
 * @param oras
 * @param strada
 * @param nr
 * @throws SQLException
 * @throws ClassNotFoundException
 * Metoda adauga un client in baza de date, datele despre el sunt impartite in 2 tablele: Client si Adresa
 */

	public void addClient (String nume, String prenume, String telefon, String email, String sex, String codPostal, String judet, String oras, String strada, String nr) throws SQLException, ClassNotFoundException
	{
		String query= String.format("insert into  Client (nume, prenume, telefon, email, sex) values ('%s','%s','%s','%s','%s'); ",nume, prenume,telefon,email,sex);
		Connection con= Conexiune.getConnection();
		Statement stm=null;
		ResultSet rs=null;
		stm = con.createStatement();
		stm.executeUpdate(query);
		query=String.format("select id from Client where nume='%s';",nume);
		rs=stm.executeQuery(query);
		rs.next();
		query=String.format("insert into Adresa (idClient, codPostal,judet,oras,strada,numar) values ('%d','%s','%s','%s','%s','%s');", Integer.parseInt(rs.getString(1)),codPostal,judet,oras,strada,nr);
		stm.executeUpdate(query);
		stm.close();
		rs.close();
		con.close();
	}
	
/**
 * 	
 * @param id
 * @return
 * @throws SQLException
 * @throws ClassNotFoundException
 * Verifica daca un client cu un anumit id se afla in baza de date
 */
	public int validareClient(String id) throws SQLException, ClassNotFoundException{
		Connection con= Conexiune.getConnection();
		Statement stm=null;
		ResultSet rs=null;
		String query= String.format("select id from Client where id='%s';",id);
		stm=con.createStatement();
		rs=stm.executeQuery(query);
		if(rs.next()==false)
				{
				JOptionPane.showMessageDialog(null, "Clientul nu a fost gasit!");
				return-1;
				}
		stm.close();
		rs.close();
		con.close();
		return 0;
	}
	
/**
 * Stergerea unui client, se sterg  datele despre clientul respectiv din tabelele Adresa, Client,	Comanda si Detalii 
 * @param id
 * @throws SQLException
 * @throws ClassNotFoundException
 */
	public void deleteClient(String id) throws SQLException, ClassNotFoundException {
		Connection con= Conexiune.getConnection();
		ResultSet rs1=null;
		if(validareClient(id)==-1)return;
		String query= String.format("select id from Adresa where idClient='%s';",id);
		Statement stm=con.createStatement();
		Statement stm1=con.createStatement();
		Statement stm2=con.createStatement();
		ResultSet rs=stm.executeQuery(query);
		if(rs.next()==true)
		{query= String.format("delete from Adresa where id='%d';", rs.getInt("id"));
		stm.execute(query);}
		query= String.format("select id from Comanda where idClient='%s';",id);
		stm=con.createStatement();
		rs=stm.executeQuery(query);
		while(rs.next()==true){
			int idComanda= rs.getInt("id");
			System.out.println("idComanda:"+idComanda);
			String query1= String.format("select id from Detalii where idComanda='%d';",idComanda);
			rs1 =stm1.executeQuery(query1);
			while(rs1.next()==true){
				query= String.format("delete from Detalii where idComanda='%d';", idComanda);
				stm2.execute(query);}
			query= String.format("delete from Comanda where id='%d';", idComanda);
			stm2.execute(query);}
		if(rs1!=null)rs1.close();
		query=String.format("delete from Client where id='%s';", id);
		stm.execute(query);
		stm.close();
		rs.close();
		con.close();}
	
	/**
	 * La un apel al metodei se sterge un singur camp 
	 * @param id
	 * @param camp
	 * @param nouaValoare
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	
	public  void editClient(String id, String camp, String nouaValoare) throws SQLException, ClassNotFoundException  {
		Connection con= Conexiune.getConnection();
		Statement stm=null;
		String query="";
		if(camp.equals("Nume")) query=String.format("update Client set Client.nume='%s' where Client.id='%s' ;",nouaValoare,id);
		if(camp.equals("Prenume")) query=String.format("update Client set Client.prenume='%s' where Client.id='%s' ;",nouaValoare,id);
		if(camp.equals("Telefon")) query=String.format("update Client set Client.telefon='%s' where Client.id='%s' ;",nouaValoare,id);
		if(camp.equals("Email")) query=String.format("update Client set Client.email='%s' where Client.id='%s' ;",nouaValoare,id);
		if(camp.equals("Sex")) query=String.format("update Client set Client.sex='%s' where Client.id='%s' ;",nouaValoare,id);
		if(camp.equals("Cod postal")) query=String.format("update Adresa set Adresa.codPostal='%s' where Adresa.idClient='%s' ;",nouaValoare,id);
		if(camp.equals("Judet")) query=String.format("update Adresa set Adresa.judet='%s' where Adresa.idClient='%s' ;",nouaValoare,id);
		if(camp.equals("Oras")) query=String.format("update Adresa set Adresa.oras='%s' where Adresa.idClient='%s' ;",nouaValoare,id);
		if(camp.equals("Strada")) query=String.format("update Adresa set Adresa.strada='%s' where Adresa.idClient='%s' ;",nouaValoare,id);
		if(camp.equals("Numar")) query=String.format("update Adresa set Adresa.numar='%s' where Adresa.idClient='%s' ;",nouaValoare,id);
		if(query.equals("")) JOptionPane.showMessageDialog(null, "Clientul nu a fost gasit!");
		stm=con.createStatement();
		if(stm.executeUpdate(query)==0)JOptionPane.showMessageDialog(null, "Clientul nu a fost gasit!");
		stm.close();
		con.close();
	}
	
	/**
	 * Inserarea unui noi rand in tabela Produs
	 * @param nume
	 * @param cantitate
	 * @param pret
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	
	public  void addProdus (String nume, String cantitate, String pret) throws SQLException, ClassNotFoundException{

		String query= String.format("insert into  Produs (nume, cantitate, pret) values ('%s','%s','%s'); ",nume, cantitate, pret);
		Connection con= Conexiune.getConnection();
		Statement stm=null;
		stm = con.createStatement();
		stm.executeUpdate(query);
		stm.close();
		con.close();
	}
	
	/**
	 * Se sterg informatiile despre produsul cu numele nume, din tabelele Produs, Detalii si Comanda
	 * @param nume
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void deleteProdus(String nume) throws SQLException, ClassNotFoundException {
		Connection con= Conexiune.getConnection();
		Statement stm=null;
		ResultSet rs=null,rs1=null;
		String query=String.format("select id from Produs where nume='%s' ; ",nume);
		stm=con.createStatement();
		rs=stm.executeQuery(query);
		if(rs.next()==true)
		{	int idProdus=rs.getInt(1);
			ArrayList <Integer>idComanda=new ArrayList<Integer>();
			query=String.format("select idComanda from Detalii where idProdus='%d' ;",idProdus);
			rs1=stm.executeQuery(query);
			while(rs1.next()==true) {
				idComanda.add(new Integer(rs1.getInt(1)));
			}
			query=String.format("delete from Detalii where idProdus='%d' ;", idProdus);
			while(stm.execute(query)) {}
			for(Integer i:idComanda){
				query=String.format("delete from Comanda where id='%d';", i);
				stm.execute(query);
			}
			rs1.close();
		}
		else {JOptionPane.showMessageDialog(null, "Produsul nu a fost gasit!");
			stm.close();
			rs.close();
			return;}
		query=String.format("delete from Produs where nume='%s' ;",nume);
		stm=con.createStatement();
		stm.execute(query);
		stm.close();
		rs.close();
		con.close();
	}
	
	/**
	 * Se poate modifica un singur camp al tabelului la un apel al metodei
	 * @param nume
	 * @param camp
	 * @param nouaValoare
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public  void editProdus (String nume, String camp, String nouaValoare)throws SQLException, ClassNotFoundException {
		Connection con= Conexiune.getConnection();
		Statement stm=null;
		String sql="";
		if(camp.equals("Nume")) sql=String.format("update Produs set Produs.nume='%s' where Produs.nume='%s' ;",nouaValoare,nume);
		if(camp.equals("Cantitate")) sql=String.format("update Produs set Produs.cantitate='%d' where Produs.nume='%s' ;",Integer.parseInt(nouaValoare),nume);
		if(camp.equals("Pret")) sql=String.format("update Produs set Produs.pret='%d' where Produs.nume='%s' ;",Integer.parseInt(nouaValoare),nume);
		stm=con.createStatement();
		if(stm.executeUpdate(sql)==0)JOptionPane.showMessageDialog(null, "Produsul nu a fost gasit!");
		stm.close();
		con.close();
	}
	
}
