package Model;

import java.awt.Component;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DataAccess.Conexiune;

public class Client {
	private int id;
	private String nume;
	private String prenume;
	private String telefon;
	private String email;
	private String sex;
	
	public Client() {
		
	}
	public Client(int id, String nume, String prenume, String telefon ,String email, String sex)
	{
		this.id=id;
		this.nume=nume;
		this.prenume=prenume;
		this.telefon=telefon;
		this.email=email;
		this.sex=sex;
	}
	
public ArrayList<Client> getClienti() throws ClassNotFoundException, SQLException {
	ArrayList<Client> cl= new ArrayList<Client>();
	String query=String.format("select * from Client;");
	Connection con= Conexiune.getConnection();
	Statement stm=con.createStatement();
	ResultSet rs=stm.executeQuery(query);
	while(rs.next()) {
		cl.add(new Client(rs.getInt("id"),rs.getString("nume"),rs.getString("prenume"),rs.getString("telefon"),rs.getString("email"),rs.getString("sex")));
	}
	stm.close();
	con.close();
	rs.close();
	return cl;
	}


public Client getCl(int i) throws ClassNotFoundException, SQLException {
	ArrayList<Client> cl= new ArrayList<Client>();
	String query=String.format("select * from Client;");
	Connection con= Conexiune.getConnection();
	Statement stm=con.createStatement();
	ResultSet rs=stm.executeQuery(query);
	while(rs.next()) {
		cl.add(new Client(rs.getInt("id"),rs.getString("nume"),rs.getString("prenume"),rs.getString("telefon"),rs.getString("email"),rs.getString("sex")));
	}
	stm.close();
	con.close();
	rs.close();
	return cl.get(i);
}

public int getId() {
	return id;
}

public String getNume() {
	return nume;
}


public String getPrenume() {
	return prenume;
}

public String getTelefon() {
	return telefon;
}

public String getEmail() {
	return email;
}

public String getSex() {
	return sex;
}

}
