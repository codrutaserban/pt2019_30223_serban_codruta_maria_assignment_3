package Model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DataAccess.Conexiune;

public class Comanda {
	private int id;
	private String dataLivrare;
	private String dataEfectuare;
	private float total;
	
	public Comanda(){
		
	}
	Comanda(int id, String dl, String de,float total)
	{
		this.id=id;
		this.dataLivrare=dl;
		this.dataEfectuare=de;
		this.total=total;
	}
	
	public ArrayList<Comanda> getComenzi() throws ClassNotFoundException, SQLException {
		ArrayList<Comanda> c= new ArrayList<Comanda>();
		String query=String.format("select * from Comanda;");
		Connection con= Conexiune.getConnection();
		Statement stm=con.createStatement();
		ResultSet rs=stm.executeQuery(query);
		while(rs.next()) {
			c.add(new Comanda(rs.getInt("id"),rs.getString("dataLivrare"),rs.getString("dataEfectuare"),(float)rs.getDouble("total")));
		}
		stm.close();
		con.close();
		rs.close();
		return c;
		}


	public Comanda getComanda(int i) throws ClassNotFoundException, SQLException {
		ArrayList<Comanda> c= new ArrayList<Comanda>();
		String query=String.format("select * from Comanda;");
		Connection con= Conexiune.getConnection();
		Statement stm=con.createStatement();
		ResultSet rs=stm.executeQuery(query);
		while(rs.next()) {
			c.add(new Comanda(rs.getInt("id"),rs.getString("dataLivrare"),rs.getString("dataEfectuare"),(float)rs.getDouble("total")));
		}
		stm.close();
		con.close();
		rs.close();
		return c.get(i);
	}
	
	
	
	public int getId() {
		return id;
	}
	public String getDataLivrare() {
		return dataLivrare;
	}
	public String getDataEfectuare() {
		return dataEfectuare;
	}
	public float getTotal() {
		return total;
	}

}
