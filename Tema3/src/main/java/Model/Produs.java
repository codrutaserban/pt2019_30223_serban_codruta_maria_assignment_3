package Model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DataAccess.Conexiune;

public class Produs {
	private int id;
	private String nume;
	private int cantitate;
	private float pret;
 
	public Produs(){
		
	}
	
	public Produs(int id, String nume,int cantitate ,int pret)
	{this.id=id;
	this.nume=nume;
	this.cantitate=cantitate;
	this.pret=pret;}
	
	
	public ArrayList<Produs> getProduse() throws ClassNotFoundException, SQLException {
		ArrayList<Produs> pr= new ArrayList<Produs>();
		String query=String.format("select * from Produs;");
		Connection con= Conexiune.getConnection();
		Statement stm=con.createStatement();
		ResultSet rs=stm.executeQuery(query);
		while(rs.next()) {
			pr.add(new Produs(rs.getInt("id"),rs.getString("nume"),rs.getInt("cantitate"),rs.getInt("pret")));
		}
		stm.close();
		con.close();
		rs.close();
		return pr;
		}


	public Produs getProdus(int i) throws ClassNotFoundException, SQLException {
		ArrayList<Produs> pr= new ArrayList<Produs>();
		String query=String.format("select * from Produs;");
		Connection con= Conexiune.getConnection();
		Statement stm=con.createStatement();
		ResultSet rs=stm.executeQuery(query);
		while(rs.next()) {
			pr.add(new Produs(rs.getInt("id"),rs.getString("nume"),rs.getInt("cantitate"),rs.getInt("pret")));
		}
		stm.close();
		con.close();
		rs.close();
		return pr.get(i);
	}

	public int getId() {
		return id;
	}

	public String getNume() {
		return nume;
	}

	public int getCantitate() {
		return cantitate;
	}

	public float getPret() {
		return pret;
	}

}
