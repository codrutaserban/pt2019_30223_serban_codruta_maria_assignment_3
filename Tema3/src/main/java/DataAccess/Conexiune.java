package DataAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Conexiune {
	
	public static Connection getConnection() throws SQLException, ClassNotFoundException {
		Connection con=null;
		try
		{
			Class.forName("com.mysql.cj.jdbc.Driver");
			con= (Connection)DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/magazin","root","");
		}
		catch (Exception e)
		{
			System.out.println(con);
			System.err.println("error connection");
		}
		return con;
	}

	public static void closeConnection(Connection con) throws SQLException {
		try{
			con.close();
		}
		catch (Exception e)
		{
			System.err.println("error close connectin");
		}
	}
	
	
}
